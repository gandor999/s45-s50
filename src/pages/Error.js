// import { Link } from 'react-router-dom';
// import { Row, Col } from 'react-bootstrap';

// // Activity - s48

// export default function ErrorPage(){
// 	return (
// 		<Row>
// 			<Col className="p-5">
// 				<h1>Page Not Found</h1>
// 				<p>Go back to the <Link to="/">homepage</Link></p> 
// 			</Col>
// 		</Row>
// 	)
// }



// Alternate solution
import Banner from '../components/Banner';

export default function Error(){
	const data = {
		title: "Page Not Found",
		content: "Go back to the ",
		destination: "/",
		label: "homepage"
	}

	return (
		<Banner data={data} />
	)
}