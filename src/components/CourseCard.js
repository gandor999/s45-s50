import { useState } from 'react';
// Proptypes - used to validate props
import PropTypes from 'prop-types';
import { Button, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';




// There are two ways to render a prop function



// One way is to issue props as the paramater

// export default function CourseCard(props){
// 	console.log(props);
	
// 	return (
// 		<Row className="mt-3 mb-3">
// 			<Col>
// 				<Card className="cardHighlight">
// 				  <Card.Body className="">

// 				  	{/*As you can see here the props parameter cannot directly access the object properties passed but has to call on the attribute that was passed on to the function. If you also notice the attribute became a property instead to the parameter*/}
// 				    <Card.Title className=""><h5>{props.courseProp[1].name}</h5></Card.Title>

// 					{/*We can conclude that passing attributes to a function written in a manner in HTML becomes a property of the paramater of the function; the parameter is not assigned as the argument that was passed.*/}

// 				    <Card.Text>
// 				      <ul>
// 				      	<li><strong>Description:</strong></li>
// 				      	<li>{props.courseProp[1].description}</li>
// 				      </ul>

// 				      <ul>
// 				      	<li><strong>Price:</strong></li>
// 				      	<li>PhP {props.courseProp[1].price}</li>
// 				      </ul>
				      
// 				    </Card.Text>
// 				    <Button variant="primary">Enroll</Button>
// 				  </Card.Body>
// 				</Card>
// 			</Col>
// 		</Row>
// 	)
// }




// Another way is to deconstruct the argument object that was passed from the Courses.js


export default function CourseCard({courseProp}){
	// console.log(props);

	/*return (
		<Row className="mt-3 mb-3">
			<Col>
				<Card className="cardHighlight">
				  <Card.Body className="">
				    <Card.Title className=""><h5>{courseProp[1].name}</h5></Card.Title>
				    <Card.Text>
				      <ul>
				      	<li><strong>Description:</strong></li>
				      	<li>{courseProp[1].description}</li>
				      </ul>

				      <ul>
				      	<li><strong>Price:</strong></li>
				      	<li>PhP {courseProp[1].price}</li>
				      </ul>
				      
				    </Card.Text>
				    <Button variant="primary">Enroll</Button>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)*/



	// We can also decontruct the courseProp so that we don't have to write a long code

	const {_id, name, description, price } = courseProp;



	// State Hook - used to keep track of information related to individual components

	// Syntax: const [getter, setter] = useState(intitialGetterValue);

	// const [count, setCount] = useState(0); 

	// function enroll(){
	// 	setCount(count + 1);
	// }



	// return (
	// 	<Row className="mt-3 mb-3">
	// 		<Col>
	// 			<Card className="cardHighlight">
	// 			  <Card.Body className="">
	// 			    <Card.Title className=""><h5>{name}</h5></Card.Title>
	// 			    <Card.Text>
	// 			      <ul>
	// 			      	<li><strong>Description:</strong></li>
	// 			      	<li>{description}</li>
	// 			      </ul>

	// 			      <ul>
	// 			      	<li><strong>Price:</strong></li>
	// 			      	<li>PhP {price}</li>
	// 			      </ul>

	// 			      <ul>
	// 			      	<li><strong>Enrollees:</strong></li>
	// 			      	<li>{count}</li>
	// 			      </ul>
				      
	// 			    </Card.Text>
	// 			    <Button variant="primary" onClick={enroll}>Enroll</Button>
	// 			  </Card.Body>
	// 			</Card>
	// 		</Col>
	// 	</Row>
	// )




	// Activity - s46

	/*const [count, setCount] = useState(0); 

	function enroll(){

		if(count < 30){
			setCount(count + 1);
		}

		else{
			alert("There are no more seats available");
		}
	}*/



	return (
		<Row className="mt-3 mb-3">
			<Col>
				<Card className="cardHighlight">
				  <Card.Body className="">
				    <Card.Title className=""><h5>{name}</h5></Card.Title>
				    <Card.Text>
				      <ul>
				      	<li><strong>Description:</strong></li>
				      	<li>{description}</li>
				      </ul>

				      <ul>
				      	<li><strong>Price:</strong></li>
				      	<li>PhP {price}</li>
				      </ul>

				      <Link className="btn btn-primary" to={`/courses/${_id}`}>
				      	Details
				      </Link>

				      {/*<ul>
				      	<li><strong>Enrollees:</strong></li>
				      	<li>{count}</li>
				      </ul>*/}
				      
				    </Card.Text>
				    {/*<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}




// Checks the validity of the PropTypes
CourseCard.propTypes = {
	// "shape" method is used to check if a prop object conforms to a specific shape
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
